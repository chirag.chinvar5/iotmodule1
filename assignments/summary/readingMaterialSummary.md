# Industrial IoT 

#### Industrial Revoution: History
* Industry 1.0 - Weave Loming,Mechanization,Steam Power
* Industry 2.0 - Mass production began with the help of a production line
* Industry 3.0 - This Revoution saw the evolution of electronics with automation etc.
* Industry 4.0 - The sensors and acutators were able to send data to the cloud using certain mechanisms.

#### Stages of Industrial Revoution 
![flow chart](https://www.accountancysa.org.za/wp-content/uploads/2020/02/Screenshot-2020-02-26-at-13.19.30-1024x566.png)

#### Industry 3.0
* One of the major highlight of this revolution is the capablity of storing the acquried data from sensors in databases

* There are many protocols to send the data to a server for further processing few are 
 * ModBUS
 * CanOpen
 * ETHERCat

* The devices of this revoution had the following architecture 
    * Field devices
        * sensors
        * acutators
        * Motors
    
    * Control devices
        * Microcontrollers
        * Computer Numeric Controls

    * Stations
    * WorkCentres
        * Use softwares like scada,excel to store data 
    * Enterprises

    * The field bus connects the sensors and controllers.

#### Industrial Revoution 4
* This revolution sets a mark by enabling the sensors to send data over the cloud to certain servers using a few mechanisms and protocols
* This revolution is also known due to its vast features such as 
    * To create realtime dashboard to collect and summarise data in one place
    * Get realtime time alerts and notifications due to mishaps
    * The devices can be configured remotely
    * Regular maintainenace is possible.

![i4](https://www.controleng.com/wp-content/uploads/sites/2/2014/10/CTL1411_WEB_F4_Industry4pt0_Beckhoff_TC3_SOA_OPC-UA_x2-B2B-B2M-M2M-w485x350.jpg)

* Some protocols used to send data to the cloud are
    * MQTT
    * Web Sockets
    * https
    * REST API

![protocols](https://data-flair.training/blogs/wp-content/uploads/sites/2/2018/06/IoT-Protocol.jpg)

* With this increased features also comes added disadvantages such as
    * Reliablity- If our connection is not secure it becomes vulnerable to hackers
    * Down time - Due to overloading of servers at stations there are chances the network might experience a down time
    * Cost - Connecting devices requries additional hardware which comes at a cost.

#### Industry 3.0 to Industry 4.0
* This poses a very reliable method due to very few alterations in the present processes
* This is achieved by the flow chart below

![3.0 to 4.0](https://techinsight.com.vn/wp-content/uploads/2018/07/4-1.png)

* There a few challenges faced during this conversion
    * Expensive hardware
    * Lack of documentation

#### Processing data
* Once the data is recieved from the sensor to the cloud it can be processed for futher use.
* Processing the data requries the data to be stored 
* For this various TSDB over traditional databases
* TSDB is preferred 
    * It affixs a timestamp to the incoming fields.
    * The fields dont have to be predefined like traditional SQL databases

* Various TSDB providers are :
    * INFLUX db
    * Promethus
    * Things Board
    * Grafana(Analytics tool for analysis)

* These data can be stored in IoT platforms like 
    * AWS
    * Azure
    * Google Firebase

![iot platforms](https://devopedia.org/images/article/86/2438.1528650763.jpg)

* Alerts or notifications can be recieved using services like
    * Twilio
    * Zapier
    * Integromat
    * IFTTT